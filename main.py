from operaciones2 import Resta, Division

def ComprobarInput(a):
	try:
		return int(a)
	except:
		valor= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInput(valor)

def ComprobarInputDeno(a):
	try:
		if int(a)!=0:
			return int(a)
		elif int(a)==0:
			valor1= input("Error, el valor debe ser mayor a 0, Ingrese nuevamente el valor: ")
			return ComprobarInputDeno(valor1)
	except:
		valor2= input("Error Ingrese nuevamente el valor: ")
		return ComprobarInputDeno(valor2)

def Rebobinar():
	print("Deseas realizar otra operacion: ")
	print("1)Si")
	print("2)No")
	Den = input("Ingrese opcion: ")
	ComDen = ComprobarInput(Den)

	if ComDen == 1:
		main()
	elif ComDen == 2:
		print("Adios....")
	else:
		print("Error en el valor ingresado")
		Rebobinar()

def main():
	print("-Operaciones-")
	print("1) Suma")
	print("2) Resta")
	print("3) Multiplicacion")
	print("4) Division")
	opc = input("Ingrese el numero de la opcion de la operacion que desea realizar: ") 	
	if opc =="1":
		pass
	elif opc =="2":
		a= input("Ingrese primer valor: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo valor: ")
		CompB = ComprobarInput(b)
		Resta(CompA,CompB)
		Rebobinar()
	elif opc =="3":
		pass
	elif opc =="4":
		a= input("Ingrese valor del numerador: ")
		CompA = ComprobarInput(a)
		b= input("Ingrese segundo denominador: ")
		CompB = ComprobarInputDeno(b)
		Division(CompA,CompB)
		Rebobinar()
	else:
		print("Error en la opcion ingresado")
		main()


print("-------------------Calculadora Basica-------------------")
main()